#include "changelogtableview.h"

#include <QHeaderView>

#include "movement.h"
#include "UI/mainwindow.h"
#include "vertexchangelog.h"

#include <iostream>


ChangelogTableView::ChangelogTableView(QWidget* parent) : QTableView(parent){
  movementChangelog = new VertexChangelog(this);
  initialise_changelog();
}

ChangelogTableView::ChangelogTableView(Mesh* mesh, QWidget* parent) : QTableView(parent)
{
  movementChangelog = new VertexChangelog(this);
  activeMesh = mesh;
  initialise_changelog();

}

void ChangelogTableView::initialise_changelog(){
  setModel(movementChangelog);
  horizontalHeader()->setStretchLastSection(true);
  verticalHeader()->setHidden(true);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectRows);
  setFocusPolicy(Qt::ClickFocus);
}

void ChangelogTableView::reinitialiseTable(Mesh* newMesh){
  movementChangelog->clearList();
  activeMesh = newMesh;
  std::cout << "loaded new mesh" << std::endl;
}

void ChangelogTableView::addVertexMovement(int vertexNumber){
  Movement newMovement = Movement(vertexNumber, activeMesh->getVertexFromNumber(vertexNumber)->previousPos , activeMesh->getVertexFromNumber(vertexNumber)->pos());
  movementChangelog->addMovement(newMovement, QModelIndex());
  resizeColumnsToContents();
  resizeRowsToContents();

}

void ChangelogTableView::addVertexInChangelog(int vertexNumber){
  Movement newMovement = Movement(vertexNumber, activeMesh->getVertexFromNumber(vertexNumber)->pos(), Movement::addVertex);
  movementChangelog->addMovement(newMovement, QModelIndex());
  resizeColumnsToContents();
  resizeRowsToContents();
}

void ChangelogTableView::deleteVertexInChangelog(int vertexNumber){
  Movement newMovement = Movement(vertexNumber, activeMesh->getVertexFromNumber(vertexNumber)->pos(), Movement::deleteVertex);
  movementChangelog->addMovement(newMovement, QModelIndex());
  resizeColumnsToContents();
  resizeRowsToContents();

}

void ChangelogTableView::addEdgeInChangelog(int startVertexNumber, int endVertexNumber){
  Movement newMovement = Movement(startVertexNumber, endVertexNumber, Movement::addEdge);
  movementChangelog->addMovement(newMovement, QModelIndex());
  resizeColumnsToContents();
  resizeRowsToContents();
}

void ChangelogTableView::deleteEdgeInChangelog(int startVertexNumber, int endVertexNumber){
  Movement newMovement = Movement(startVertexNumber, endVertexNumber, Movement::deleteEdge);
  movementChangelog->addMovement(newMovement, QModelIndex());
  resizeColumnsToContents();
  resizeRowsToContents();
}
