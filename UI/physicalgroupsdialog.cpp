#include "physicalgroupsdialog.h"
#include "ui_physicalgroupsdialog.h"

#include <iostream>

#include "Mesh/mesh.h"
#include "Mesh/meshelement.h"
#include "Mesh/physicalgroup.h"

PhysicalGroupsDialog::PhysicalGroupsDialog(QWidget *parent, int groupType, Mesh* parentMesh) :
  QDialog(parent),
  ui(new Ui::PhysicalGroupsDialog)
{
  ui->setupUi(this);
  mesh = parentMesh;
  findPhysicalGroups(groupType);
  if (groupCheckboxes.empty()){
    ui->label_2->setHidden(true);
  }
}

PhysicalGroupsDialog::~PhysicalGroupsDialog()
{
  delete ui;
}

void PhysicalGroupsDialog::findPhysicalGroups(int groupType){
  std::for_each(mesh->physGroups.begin(), mesh->physGroups.end(), [groupType, this](const PhysicalGroup* obj) {if (obj->elementType == groupType){  addGroupCheckbox(obj->groupName); };});
}

void PhysicalGroupsDialog::addGroupCheckbox(QString groupName){
  QCheckBox* newCheckBox = new QCheckBox(groupName, this);
  ui->verticalLayout_2->addWidget(newCheckBox);
  groupCheckboxes.push_back(newCheckBox);
  std::cout << groupName.toStdString() << std::endl;
}

QString PhysicalGroupsDialog::getNewGroupName(){
  return ui->lineEdit->text();
}
