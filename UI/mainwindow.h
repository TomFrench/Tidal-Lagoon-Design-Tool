#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QGraphicsScene>
#include "Mesh/mesh.h"


class Field;
class FieldEditor;

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  void loadMeshFile();
  Mesh* getActiveMesh();
  MeshView* getMeshView();


  /*
   * Queries SideBar for contents of Prepend_Text_textEdit and returns it
   */
  QString getPrependText();

  /*
   * Queries SideBar for contents of Append_Text_textEdit and returns it
   */
  QString getAppendText();

  void writeUnknownText(QString text);
  FieldEditor* getFieldEditor();


signals:

  void meshSizeChanged(int vertexCount);

  void activeVertexChanged(int vertexNumber);

  ///////////////////////////////////////////
  // Changelog functions
  ///////////////////////////////////////////

  void addVertexMovement(int vertexNumber);

  void addVertexInChangelog(int vertexNumber);

  void deleteVertexInChangelog(int vertexNumber);

  void addEdgeInChangelog(int startVertexNumber, int endVertexNumber);

  void deleteEdgeInChangelog(int startVertexNumber, int endVertexNumber);

  ///////////////////////////////////////////
  // View functions
  ///////////////////////////////////////////

  void setViewRotation(int rotationAngle);

  void rotateView(int rotationAngle);

  void refreshVertexCoords(int vertexNumber);


public slots:


  ///////////////////////////////////////////
  // Mesh Info functions
  ///////////////////////////////////////////

  void updateMeshInfo();
  void updateProgressBar(double progress);



private slots:

  ///////////////////////////////////////////
  // Menubar slots
  ///////////////////////////////////////////


  /*
   * Selects a mesh file to be read into activeMesh
   */

  void on_actionOpen_File_triggered();

  /*
   * Saves the mesh to chosen location
   */
  void on_actionSave_File_triggered();

  ///////////////////////////////////////////
  // View functions
  ///////////////////////////////////////////


  void on_actionRotate_90_clockwise_triggered();

  void on_actionRotate_90_anticlockwise_triggered();

  void on_actionReset_Rotation_triggered();

  void on_actionRedo_triggered();

  void on_actionUndo_triggered();

  void on_actionToggle_Sidebar_triggered();

  void on_actionInsert_mesh_triggered();

  void on_actionSave_Mesh_triggered();

  void on_actionRelax_Mesh_triggered();

  void on_actionNew_Mesh_triggered();

private:
  Ui::MainWindow *ui;

  QGraphicsScene scene;


};

#endif // MAINWINDOW_H
