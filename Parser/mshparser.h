#ifndef MSHPARSER_H
#define MSHPARSER_H

#include <string>

class Mesh;
class MeshView;

class MshParser
{
public:
  MshParser();

  Mesh* parse(std::string meshFile, MeshView* view);

};

#endif // MSHPARSER_H
