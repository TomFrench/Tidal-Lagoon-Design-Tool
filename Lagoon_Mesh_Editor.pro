#-------------------------------------------------
#
# Project created by QtCreator 2017-07-18T11:09:25
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lagoon_Mesh_Editor
TEMPLATE = app


SOURCES += main.cpp\
    UI/changelogtableview.cpp \
    UI/mainwindow.cpp \
    UI/sidebar.cpp \
    UI/fieldeditor.cpp \
    UI/physicalgroupsdialog.cpp \
    Mesh/mesh.cpp \
    Mesh/meshelement.cpp \
    Mesh/vertex.cpp \
    Mesh/linesection.cpp \
    Mesh/meshedge.cpp \
    Mesh/spline.cpp \
    Mesh/physicalgroup.cpp \
    Mesh/Fields/field.cpp \
    Mesh/Fields/attractorfield.cpp \
    Mesh/Fields/thresholdfield.cpp \
    Parser/geoparser.cpp \
    Parser/geowriter.cpp \
    Parser/mshparser.cpp \
    Parser/mshwriter.cpp \
    meshview.cpp \
    movement.cpp \
    vertexchangelog.cpp


HEADERS  += UI/mainwindow.h\
    UI/changelogtableview.h \
    UI/sidebar.h \
    UI/fieldeditor.h \
    UI/physicalgroupsdialog.h \
    Mesh/mesh.h \
    Mesh/meshelement.h \
    Mesh/vertex.h \
    Mesh/linesection.h \
    Mesh/meshedge.h \
    Mesh/spline.h \
    Mesh/physicalgroup.h \
    Mesh/Fields/field.h \
    Mesh/Fields/attractorfield.h \
    Mesh/Fields/thresholdfield.h \
    Parser/geoparser.h \
    Parser/geowriter.h \
    Parser/mshparser.h \
    Parser/mshwriter.h \
    meshview.h \
    movement.h \
    vertexchangelog.h



CONFIG += c++11

LIBS += -lboost_system \
        -lboost_filesystem \
        -lpython2.7


FORMS    += UI/mainwindow.ui \
    UI/sidebar.ui \
    UI/physicalgroupsdialog.ui
