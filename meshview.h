#ifndef MESHVIEW_H
#define MESHVIEW_H

#include <QGraphicsView>
#include <QTimer>
#include <memory>

class Vertex;
class Mesh;
class MeshEdge;

class MeshView : public QGraphicsView
{
  Q_OBJECT

public:
  /*
   * Constructor
   */
  MeshView(QWidget* parent = 0);

  /*
   * Destructor
   */
  ~MeshView();

  ///////////////////////////////////////////
  // Loading Meshes
  ///////////////////////////////////////////

  /*
   * Loads a new mesh
   */
  bool loadMesh();
  bool loadMesh(QString filePath);
  bool loadMesh(std::string filePath);

  void loadEmptyMesh();

  bool insertMesh();
  bool clearMesh();

  void setActiveMesh(Mesh* newMesh);

  ///////////////////////////////////////////
  // "Get" Functions
  ///////////////////////////////////////////

  /*
   * Returns the mesh currently being displayed
   */
  Mesh* getActiveMesh();

  Vertex* getActiveVertex();

  ///////////////////////////////////////////
  // Vertex/Line Addition/Deletion
  ///////////////////////////////////////////

  void addVertexToScene(Vertex* newVertex);

  void addLineToScene(std::shared_ptr<MeshEdge> newLine);

  ///////////////////////////////////////////
  // Zoom/Rotate
  ///////////////////////////////////////////

  double scalefactor = 1;
  void zoom(double zoomFactor);

  ///////////////////////////////////////////
  // View Mode functions
  ///////////////////////////////////////////

  int viewMode = EditMode;
  enum { EditMode = 0 };
  enum { SelectionMode = 1 };

  std::vector<Vertex*> selectedVertices;

signals:

  /*
   * Signals that the view has been zoomed
   * used to resize vertices and edges
   */
  void viewZoomed(double scaleFactor);

  void updateActiveVertex(int vertexNumber);

  void newMeshLoaded(Mesh* newMesh);

public slots:

  ///////////////////////////////////////////
  // Mesh drawing functions
  ///////////////////////////////////////////

  /*
   * Resets size of vertices and edges and sets them all visible
   */
  void refreshView();

  ///////////////////////////////////////////
  // View navigation functions
  ///////////////////////////////////////////

  /*
   * Tracks for movement of the mousewheel
   * This is then used to zoom the view
   */
  void wheelEvent(QWheelEvent *event);

  /*
   * Resets lines to be visible after they are hidden for zooming
   */
  void setLineVisibility(bool condition);

  ///////////////////////////////////////////
  // Vertex Selection functions
  ///////////////////////////////////////////

  /*
   * Overriding default behaviour of the right mouse button
   * to give rubberband selection on click and drag
   */
  void mousePressEvent(QMouseEvent * e) override;
  void mouseMoveEvent(QMouseEvent *event) override;
  void mouseReleaseEvent(QMouseEvent * e) override;

  void setActiveVertex(int vertexNumber);

  ///////////////////////////////////////////
  // Zoom/Rotate/Reflect
  ///////////////////////////////////////////

  void rotateView(int angle);
  void setViewRotation(int angle);
  void verticalFlip();
  void horizontalFlip();


  ///////////////////////////////////////////
  // View Mode functions
  ///////////////////////////////////////////

  void setViewMode(int newViewMode);
  void cycleViewMode();



private:

  ///////////////////////////////////////////
  // Data structures
  ///////////////////////////////////////////

  Vertex* activeVertex = nullptr;

  // Currently displayed mesh file
  Mesh* activeMesh = nullptr;

  // Timer to measure when scrolling has stopped
  QTimer* zoomLineVisibilityTimer = nullptr;

  //Matrix containing the current rotation of the mesh in the view
  QTransform rotationState = QTransform();

  //Matrix containing the current reflection state of the mesh in the view
  QTransform reflectionState = QTransform();

};

#endif // MESHVIEW_H

