#ifndef MESHEDGE_H
#define MESHEDGE_H

#include <QLineF>

#include "Mesh/meshelement.h"
#include "Mesh/linesection.h"
#include "Mesh/mesh.h"
#include "Mesh/vertex.h"

class Vertex;
class Spline;
class Mesh;

class MeshEdge : public QObject, public QGraphicsLineItem, public LineSection
{
  Q_OBJECT

public:
  ///////////////////////////////////////////
  // Constructors
  ///////////////////////////////////////////

  MeshEdge(Vertex* startVertex, Vertex* endVertex, int lineID);

  ///////////////////////////////////////////
  // Data Structures
  ///////////////////////////////////////////

  //Container for the line drawn on the QQraphicsScene
  QLineF line;

  //Vertices which mark either end of the MeshEdge
  Vertex* startPoint = nullptr;
  Vertex* endPoint = nullptr;

  //Identifier for this MeshEdge
  int ID;

  //Spline which this MeshEdge is a part of.
  Spline* parentSpline = nullptr;

  ///////////////////////////////////////////
  // Data Structures
  ///////////////////////////////////////////

  /*
   * Returns the current position of startPoint
   * used to update line when vertices are moved
   */
  QPointF startPosition();

  /*
   * Returns the current position of endPoint
   * used to update line when vertices are moved
   */
  QPointF endPosition();

  /*
   * Returns ID
   */
  int getID();

  /*
   * Returns a pointer to the vertex at the beginning of the MeshEdge
   */
  Vertex* startVertex();


  /*
   * Returns a pointer to the vertex at the end of the MeshEdge
   */
  Vertex* endVertex();

  ///////////////////////////////////////////
  // MeshEdge Editing
  ///////////////////////////////////////////

  /*
   * Updates the appearance of the line on the QGraphicsScene
   * sets line colour to match that of parent spline and scales width in accordance with zoom.
   */
  void updateLine();

  /*
   * Opens a context menu for this MeshEdge
   */
  void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);



public slots:

  /*
   * Splits the MeshEdge into a number of equal sections specified by the user
   * If the MeshEdge is part of a Spline, the new vertices and MeshEdges are added to the spline.
   */
  void split(int sections);

  /*
   * Opens a dialog for user to input a number of sections
   * then calls split using that number of sections
   */
  void splitLine();

  /*
   * Calls split in order to split MeshEdge in two
   */
  void bisect();

  /*
   * Opens a dialog for the user to select which physical groups
   * to add the MeshEdge to, and adds it to these groups
   */
  bool addToPhysicalGroup();

};

#endif // MESHEDGE_H
