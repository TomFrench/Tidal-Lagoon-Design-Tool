#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>


#include "Parser/geowriter.h"
#include "Parser/mshwriter.h"
#include "Mesh/vertex.h"
#include "Mesh/mesh.h"

#include "UI/mainwindow.h"

///////////////////////////////////////////
// Constructors and Destructors
///////////////////////////////////////////

Mesh::Mesh() : QObject(){
}

Mesh::Mesh(MeshView* view) : QObject(view){
//  connect(this, SIGNAL(updateProgressBar(double)), this->parent()->parent()->parent(), SLOT(updateProgressBar(double)));
  connect(this, SIGNAL(vertexAdded(int)), view->window(), SIGNAL(addVertexInChangelog(int)));
  connect(this, SIGNAL(vertexDeleted(int)), view->window(), SIGNAL(deleteVertexInChangelog(int)));
  connect(this, SIGNAL(edgeAdded(int, int)), view->window(), SIGNAL(addEdgeInChangelog(int, int)));
  connect(this, SIGNAL(edgeDeleted(int, int)), view->window(), SIGNAL(deleteEdgeInChangelog(int, int)));
  connect(this, SIGNAL(meshSizeChanged(int)), view->window(), SIGNAL(meshSizeChanged(int)));

  fieldEditor = qobject_cast<MainWindow*>(qobject_cast<MeshView*>(parent())->window())->getFieldEditor();



}

Mesh::~Mesh(){
  for (Vertex* p : getVertices()){
    delete p;
  }
  for (Spline* p : splineList){
    delete p;
  }
  for (Spline* p : lineGroupList){
    delete p;
  }
  for (PhysicalGroup* group : physGroups){
    delete group;
  }
}

///////////////////////////////////////////
// Mesh saving
///////////////////////////////////////////

bool Mesh::saveMesh(QString savePath){

  QString fileExt = savePath.split(".").back();
  if (fileExt == "geo"){
    return saveMeshAsGeo(savePath);
  } else if (fileExt == "msh"){
    return saveMeshAsMsh(savePath);
  } else {
    std::cout << "File format not recognised" << std::endl;
    return false;
  }
}

bool Mesh::saveMeshAsGeo(QString savePath){
  GeoWriter writer;
  writer.write(savePath.toStdString(), this);
  return true;
}

bool Mesh::saveMeshAsMsh(QString savePath){
  MshWriter writer;
  writer.write(savePath.toStdString(), this);
  return true;
}

bool sortRule(const std::vector<int>& a, const std::vector<int>& b){
  return a[0] < b[0];
}

////////////////////////////////////////////////
////////////////////////////////////////////////
// Mesh construction
////////////////////////////////////////////////
////////////////////////////////////////////////

///////////////////////////////////////////
// Vertex Addition/Deletion
///////////////////////////////////////////

void Mesh::addVertex(Vertex* newVertex, bool trackChange){
  vertices.push_back(newVertex);
  qobject_cast<MeshView*>(parent())->addVertexToScene(newVertex);
  if (trackChange){
    emit vertexAdded(newVertex->vertexNumber);
    emit meshSizeChanged(vertexCount());
  }
}

void Mesh::addVertex(QPointF position, bool trackChange){
  int vertexNumber = vertexCount() + 1;
  Vertex* newVertex = new Vertex(vertexNumber, position.x(), position.y(), 0, this);
  addVertex(newVertex, trackChange);
}

bool Mesh::deleteVertex(Vertex* delVertex, bool trackChange){
  //Delete all the lines connected to the vertex
  for (std::shared_ptr<MeshEdge> p : delVertex->getLines()){
    deleteLine(p, trackChange);
  }

  //Hides the vertex so when it is removed from the scene it's appearance isn't left behind.
  delVertex->setVisible(false);

  //Remove Vertex from scene
  qobject_cast<MeshView*>(this->parent())->scene()->removeItem(delVertex);

  //Cannot remove vertex from Mesh yet as would require renumbering all vertices
  delVertex->setDeletionMarker(true);

  if (trackChange){
    emit vertexDeleted(delVertex->vertexNumber);
  }

  return true;


}

bool Mesh::collapseVertexVector(){

  //Need to remove all vertices which have been marked for deletion from the mesh
  std::vector<Vertex*> tempVertices;
  int vertexNumberOffset = 0;
  for (Vertex* p : getVertices()){
    if(p->isMarkedForDeletion()){
      vertexNumberOffset++;
    }
    else{
      p->vertexNumber = p->vertexNumber - vertexNumberOffset;
      tempVertices.push_back(p);
    }
  }
  if (tempVertices.size() < vertices.size()){
    vertices = tempVertices;
    emit meshSizeChanged(vertexCount());
    return true;
  } else {
    return false;
  }
}

///////////////////////////////////////////
// Edge Addition/Deletion
///////////////////////////////////////////

void Mesh::addLine(Vertex* startVertex, Vertex* endVertex, bool trackChange){
  //Test if edge already exists in the mesh
  std::vector<int> neighbours = startVertex->getNeighbourNumbers();
  if (std::find(neighbours.begin(), neighbours.end(), endVertex->vertexNumber) == neighbours.end() && startVertex != endVertex){
    std::shared_ptr<MeshEdge> newLine(new MeshEdge(startVertex, endVertex, lineCount()+1));
    std::cout << "Line " << newLine.get()->getID() << " (" << newLine.get()->startPoint->vertexNumber << ","
                                                         << newLine.get()->endPoint->vertexNumber << ")" << std::endl;
    newLine->updateLine();
    startVertex->addLine(newLine);
    endVertex->addLine(newLine);
    meshLines.push_back(newLine);
    qobject_cast<MeshView*>(parent())->addLineToScene(newLine);
    if (trackChange){
      emit edgeAdded(startVertex->vertexNumber, endVertex->vertexNumber);
    }
  }
}

void Mesh::addLine(int startVertexNumber, int endVertexNumber, bool trackChange){
  Vertex* startVertex = getVertexFromNumber(startVertexNumber);
  Vertex* endVertex = getVertexFromNumber(endVertexNumber);
  addLine(startVertex, endVertex, trackChange);
}

bool Mesh::deleteLine(std::shared_ptr<MeshEdge> delLine, bool trackChange){
  if (delLine != nullptr){
    //Remove line from scene
    qobject_cast<MeshView*>(this->parent())->scene()->removeItem(delLine.get());

    //Creates a new lines vector without the deleted line
    int IDOffset = 0;
    std::vector<std::shared_ptr<MeshEdge> > tempLines;
    for (std::shared_ptr<MeshEdge> p : getLines()){
      if(p != delLine){
        p.get()->ID = p.get()->ID - IDOffset;
        tempLines.push_back(p);
//        std::cout << p.get()->getID() << std::endl;
      }
      else{
         IDOffset++;
      }
    }
    meshLines = tempLines;
    //Removes line from vectors of lines attached to vertices at each end.
    delLine->startPoint->deleteLine(delLine);
    delLine->endPoint->deleteLine(delLine);

    if (trackChange){
      emit edgeDeleted(delLine->startPoint->vertexNumber, delLine->endPoint->vertexNumber);
    }
    return true;
  }
  return false;
}

bool Mesh::deleteLine(Vertex* startVertex, Vertex* endVertex, bool trackChange){
  std::shared_ptr<MeshEdge> delLine = startVertex->getLine(endVertex);
  return deleteLine(delLine, trackChange);
}

bool Mesh::deleteLine(int startVertexNumber, int endVertexNumber, bool trackChange){
  std::shared_ptr<MeshEdge> delLine = getVertexFromNumber(startVertexNumber)->getLine(getVertexFromNumber(endVertexNumber));
  return deleteLine(delLine, trackChange);
}


///////////////////////////////////////////
// LineGroup Addition/Deletion
///////////////////////////////////////////

bool Mesh::addLineGroup(int rootVertexNumber, int targetVertexNumber, QString physicalGroupName){
  Vertex* splineRootVertex = getVertexFromNumber(rootVertexNumber);
  Vertex* splineTargetVertex = getVertexFromNumber(targetVertexNumber);
  return addLineGroup(splineRootVertex, splineTargetVertex, physicalGroupName);
}

bool Mesh::addLineGroup(Vertex* rootVertex, Vertex* targetVertex, QString physicalGroupName){
  std::vector<Vertex*> vertexList;
  vertexList.push_back(rootVertex);
  vertexList.push_back(targetVertex);
  return addLineGroup(vertexList, physicalGroupName);
}

bool Mesh::addLineGroup(std::vector<Vertex*> vertexList, QString physicalGroup){
  if (vertexList.size() <= 1){
    // Only one waypoint so impossible to draw Spline
    return false;
  }
  try{
    std::cout << "Building a spline from " << vertexList.front()->vertexNumber << " to " << vertexList.back()->vertexNumber << std::endl;
    Spline* newSpline =  new Spline(vertexList, physicalGroup);
    lineGroupList.push_back(newSpline);
    return true;
  }
  catch (std::exception& e){
    // caught that spline was not valid
    // No path from rootVertex to targetVertex
    return false;
  }
  return false;
}

bool Mesh::deleteLineGroup(Spline* delSpline){
  removeFromPhysGroup(delSpline);
  splineList.erase(std::remove(splineList.begin(), splineList.end(), delSpline), splineList.end());
  delete delSpline;
  return true;
}

///////////////////////////////////////////
// Spline Addition/Deletion
///////////////////////////////////////////

bool Mesh::addSpline(int rootVertexNumber, int targetVertexNumber, QString physicalGroupName){
  Vertex* splineRootVertex = getVertexFromNumber(rootVertexNumber);
  Vertex* splineTargetVertex = getVertexFromNumber(targetVertexNumber);
  return addSpline(splineRootVertex, splineTargetVertex, physicalGroupName);
}

bool Mesh::addSpline(Vertex* rootVertex, Vertex* targetVertex, QString physicalGroupName){
  std::vector<Vertex*> vertexList;
  vertexList.push_back(rootVertex);
  vertexList.push_back(targetVertex);
  return addSpline(vertexList, physicalGroupName);
}

bool Mesh::addSpline(std::vector<Vertex*> vertexList, QString physicalGroup){
  if (vertexList.size() <= 1){
    // Only one waypoint so impossible to draw Spline
    return false;
  }
  try{
    std::cout << "Building a spline from " << vertexList.front()->vertexNumber << " to " << vertexList.back()->vertexNumber << std::endl;
    Spline* newSpline =  new Spline(vertexList, physicalGroup);
    splineList.push_back(newSpline);
    return true;
  }
  catch (std::exception& e){
    // caught that spline was not valid
    // No path from rootVertex to targetVertex
    return false;
  }
  return false;
}

bool Mesh::deleteSpline(Spline* delSpline){
  removeFromPhysGroup(delSpline);
  splineList.erase(std::remove(splineList.begin(), splineList.end(), delSpline), splineList.end());
  delete delSpline;
  return true;
}

///////////////////////////////////////////
// Physical Groups
///////////////////////////////////////////

bool Mesh::addToPhysGroup(MeshElement* item, QString groupName){
  //try to add to existing group
  if (!groupName.isEmpty()){
    std::cout << "Starting with " << physGroups.size() << " physical groups" << std::endl;
    for (PhysicalGroup* group : physGroups){

      // Test if a compatible group with the correct name exists
      if(groupName == group->groupName && item->elementType == group->elementType){

          //Test if the MeshElement already exists in this group
          if (std::find(group->elementList.begin(), group->elementList.end(), item) == group->elementList.end()){
            //If not, add MeshElement to group
            group->elementList.push_back(item);
            std::cout << "added to existing group, now contains " << group->elementList.size() << " elements" << std::endl;
            return true;
          }
          else {
            //Else, return as unable to add MeshElement to group
            std::cout << "Already present in group." << std::endl;
            return false;
          }

      }
    }
    //Make new group if fail
    PhysicalGroup* newGroup = new PhysicalGroup(groupName, item);
    physGroups.push_back(newGroup);
    std::cout << "added to new group" << std::endl;

    return true;
  }
  return false;
}

bool Mesh::removeFromPhysGroup(MeshElement* delItem, QString groupName){
  bool successFlag = false;
  std::vector<PhysicalGroup*> physGroupCopy = physGroups;
  for (PhysicalGroup* group : physGroups){
    if (groupName == group->groupName || groupName.isEmpty()){
      group->elementList.erase(std::remove(group->elementList.begin(), group->elementList.end(), delItem), group->elementList.end());
    }
    if (group->elementList.size() == 0){
      physGroups.erase(std::remove(physGroups.begin(), physGroups.end(), group), physGroups.end());
      delete group;
      successFlag = true;
    }
  }
  return successFlag;
}


///////////////////////////////////////////
// "Get" functions
///////////////////////////////////////////

Vertex* Mesh::getVertexFromIndex(unsigned int index){
  if (index < vertices.size()){
    Vertex* returnedVertex = vertices[index];
    return returnedVertex;
  };
  return 0;
}


Vertex* Mesh::getVertexFromNumber(unsigned int number){
  return getVertexFromIndex(number - 1);
}


std::shared_ptr<MeshEdge> Mesh::getLineFromIndex(unsigned int index){
  if (index < meshLines.size()){
    std::shared_ptr<MeshEdge> returnedVertex = meshLines[index];
    return returnedVertex;
  };
  return 0;
}


std::shared_ptr<MeshEdge> Mesh::getLineFromNumber(unsigned int number){
  return getLineFromIndex(number - 1);

}


std::vector<Vertex*> Mesh::getVertices(){
  return vertices;
}


std::vector<std::shared_ptr<MeshEdge> > Mesh::getLines(){
  return meshLines;
}


int Mesh::vertexCount(){
  return vertices.size();
}


int Mesh::lineCount(){
  return meshLines.size();
}


int Mesh::triCount(){
  return 0;
}


///////////////////////////////////////////
// Mesh drawing functions
///////////////////////////////////////////

void Mesh::updateVertexRadius(double circleRadius){
  vertexRadius = circleRadius;
}

bool Mesh::isHidden(){
  return hiddenFlag;
}

void Mesh::setHidden(bool hiddenStatus){
  hiddenFlag = hiddenStatus;
}
