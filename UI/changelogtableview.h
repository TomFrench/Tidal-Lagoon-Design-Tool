#ifndef CHANGELOGTABLEVIEW_H
#define CHANGELOGTABLEVIEW_H

#include <QTableView>

#include "Mesh/mesh.h"
#include "vertexchangelog.h"

class ChangelogTableView: public QTableView
{
  Q_OBJECT
public:
  ChangelogTableView();
  ChangelogTableView(QWidget* parent);
  ChangelogTableView(Mesh* mesh, QWidget* parent);

  Mesh* activeMesh = nullptr;

  void initialise_changelog();

  VertexChangelog* movementChangelog = nullptr;


public slots:

  void reinitialiseTable(Mesh* newMesh);

  void addVertexMovement(int vertexNumber);
  void addVertexInChangelog(int vertexNumber);
  void deleteVertexInChangelog(int vertexNumber);
  void addEdgeInChangelog(int startVertexNumber, int endVertexNumber);
  void deleteEdgeInChangelog(int startVertexNumber, int endVertexNumber);



};

#endif // CHANGELOGTABLEVIEW_H
