#include "Mesh/Fields/attractorfield.h"

#include "Parser/geowriter.h"

AttractorField::AttractorField(): Field()
{

}

AttractorField::AttractorField(QString fieldName,QString newedgesList, QString newfacesList, int newfieldX, int newfieldY,
                               int newfieldZ, int newnNodesByEdge, QString newnodeNumberString, bool backgroundField): Field(fieldName, backgroundField){
  nodeNumberString = newnodeNumberString;
  edgesList = newedgesList;
  facesList = newfacesList;
  nNodesByEdge = newnNodesByEdge;
  fieldX = newfieldX;
  fieldY = newfieldY;
  fieldZ = newfieldZ;
}

void AttractorField::buildNodeList(){
  nodeList.clear();
  for (int vertexNumber : nodeNumberList){
    nodeList.push_back(parentMesh->getVertexFromNumber(vertexNumber));
  }
}

QString AttractorField::printFieldInfo(int fieldIndex, GeoWriter* writer){

  QString fieldParameters;
  QString fieldIdentifier = "Field[IF + " + QString::number(fieldIndex) + "]";
  //Define type of field
  fieldParameters.append(fieldIdentifier + " = Attractor;\n");

  fieldParameters.append(fieldIdentifier + ".EdgesList = {"+ edgesList +"};\n");
  fieldParameters.append(fieldIdentifier + ".FacesList = {"+ facesList +"};\n");
  fieldParameters.append(fieldIdentifier + ".FieldX = "+ QString::number(fieldX) +";\n");
  fieldParameters.append(fieldIdentifier + ".FieldY = "+ QString::number(fieldY) +";\n");
  fieldParameters.append(fieldIdentifier + ".FieldZ = "+ QString::number(fieldZ) +";\n");
  fieldParameters.append(fieldIdentifier + ".NNodesByEdge = "+ QString::number(nNodesByEdge) +";\n");
  fieldParameters.append(fieldIdentifier + ".NodesList = {"+ nodeNumberString +"};\n");


  //Define nodes attached to field
  //QString nodeListExpression = writer->buildExpressionList(nodeList);
  //fieldParameters.append(fieldIdentifier + ".NodesList = {" + nodeListExpression + "};\n");

  return fieldParameters;
}
