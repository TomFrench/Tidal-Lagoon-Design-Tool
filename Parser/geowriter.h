#ifndef GEOWRITER_H
#define GEOWRITER_H

#include <vector>
#include <memory>

#include <QString>

class Mesh;
class MeshElement;
class Vertex;
class MeshEdge;
class Spline;
class Field;
class PhysicalGroup;


class GeoWriter
{
public:
  GeoWriter();
  ~GeoWriter();

  std::ofstream* file = nullptr;

  /*
   * Wrapper function
   * Prints out a representation of the given mesh to file
   */
  bool write(std::string savePath, Mesh* geoMesh);

  ///////////////////////////////////////////
  // Helper Functions
  ///////////////////////////////////////////

  /*
   * Uses vertexMap to return the geo file ID number for a given vertex
   */
  int mapVertexToFileIndex(Vertex* vert);

private:

  ///////////////////////////////////////////
  // Vertices
  ///////////////////////////////////////////

  /*
   * Prints a given vertex to the geo file
   */
  void printVertex(Vertex* savedVertex);

  ///////////////////////////////////////////
  // Lines/Splines
  ///////////////////////////////////////////

  /*
   * Prints a given line to the geo file
   */
  void printLine(std::shared_ptr<MeshEdge> savedLine);

  /*
   * Prints a given LineGroup to the geo file
   */
  void printLineGroup(Spline* savedLineGroup);

  /*
   * Prints a given Spline to the geo file
   */
  void printSpline(Spline* savedSpline);

  ///////////////////////////////////////////
  // Surfaces
  ///////////////////////////////////////////

  /*
   * Finds surfaces through PhysicalGroups with keyword names
   * Prints plane and physical surface to file
   */
  QString printSurfaces(Mesh* geoMesh);

  /*
   * Given a PhysicalGroup containing a loop of LineSections,
   * builds an ordered and directed list of lines in the loop
   */
  QString findLoop(PhysicalGroup loopGroup);

  ///////////////////////////////////////////
  // Fields
  ///////////////////////////////////////////

  void printFields(std::vector<Field*> fieldList);

  ///////////////////////////////////////////
  // Physical Groups
  ///////////////////////////////////////////

  /*
   * Prints out all the physical groups and their elements to the geofile
   */
  void printPhysicalGroups(Mesh* geoMesh);

  ///////////////////////////////////////////
  // Misc Printing Functions
  ///////////////////////////////////////////

  /*
   * Prints a set of text to the beginning of the save file
  */
  void printPrependedText(Mesh* geoMesh);

  /*
   * Prints a set of text to the end of the save file
   */
  void printAppendedText(Mesh* geoMesh);

  ///////////////////////////////////////////
  // Helper Functions
  ///////////////////////////////////////////

  /*
   * Constructs vertexMap and elementMap vectors
   */
  void buildVertexMap(Mesh* geoMesh);

  /*
   * Takes a vector of vertices representing a path on the mesh
   * Collapses this down so that a string of consecutive vertices is represented as a range
   * Returns a concatenated string of all of these vertices' geofile identifiers
   */
  QString buildExpressionList(std::vector<Vertex*> vertexPath);

  ///////////////////////////////////////////
  // Data structures
  ///////////////////////////////////////////

  int lineIndex = 0;
  int LineLoopIndex = 1;

  std::vector<MeshElement*> lineIDList;

  //Vector of the ID numbers of the vertices which have already been saved
  //Prevents duplicate output of vertices
  std::vector<int> savedVertices;

  //Vectors of Mesh's vertexNumbers and MeshElements
  //Allows referencing previously printed points/lines/splines
  //This is as elements are ordered the same as the identifiers used when printing out elements
  std::vector<int> vertexMap;
  std::vector<MeshElement*> elementMap;

};

#endif // GEOWRITER_H
